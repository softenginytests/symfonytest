<?php

namespace App\Services;

class PunkService 
{

    private $statusCode;
    private $errorMessage;

    public function __construct() 
    {
        $this->statusCode = 200;
        $this->errorMessage = '';
    }

    public function getStatusCode() 
    {
        return $this->statusCode;
    }

    public function getErrorMessage() 
    {
        return $$this->errorMessage;
    }

    public function getItems($filter) 
    {
        $items = array();

        try
        {
            $client = new \GuzzleHttp\Client();        
            if ($filter != '')
                $response =  $client->request("GET", "https://api.punkapi.com/v2/beers/?food=" . $filter);
            else
                $response =  $client->request("GET", "https://api.punkapi.com/v2/beers");

            $results = json_decode($response->getBody());
            
            $total = count($results);            

            for ($i = 0; $i < $total; $i++)
            {
                $item = new \stdclass();
                $item->id = $results[$i]->id;
                $item->nombre = $results[$i]->name;
                $item->descripcion = $results[$i]->description;    

                $items[] = $item;
            }        
        }
        catch (\GuzzleHttp\Exception\ClientException $e) 
        {            
            $exception = (string)$e->getResponse()->getBody();
            $exception = json_decode($exception);
            $this->errorMessage = $exception->message;
            $this->statusCode = $e->getCode();            
        }
        return $items;

    }

    public function getItem($id)
    {
        $item = new \stdclass();

        try 
        {
            $client = new \GuzzleHttp\Client();
            $response =  $client->request("GET", "https://api.punkapi.com/v2/beers/" . $id);
            $results = json_decode($response->getBody());
        
            $item->id = $results[0]->id;
            $item->nombre = $results[0]->name;
            $item->descripcion = $results[0]->description;
            $item->imagen = $results[0]->image_url;
            $item->slogan = $results[0]->tagline;
            $item->fabricacion = $results[0]->first_brewed;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) 
        {            
            $exception = (string)$e->getResponse()->getBody();
            $exception = json_decode($exception);
            $this->errorMessage = $exception->message;
            $this->statusCode = $e->getCode();            
        }
        
        return $item;
    }

}