<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Services\PunkService;
use Symfony\Component\HttpFoundation\JsonResponse;

class PunkController extends AbstractController
{
    /**
     * @Route("/items", name="items")
     */
    public function index(Request $request, PunkService $punkService): JsonResponse
    {   
        $queryFilter = $request->query->get('food');
        if (!empty($queryFilter))
            $items = $punkService->getItems($queryFilter);
        else
            $items = $punkService->getItems('') ;
        
        $response = new JsonResponse();

        $response->setStatusCode($punkService->getStatusCode());
        $response->setData($items);

        return $response;
        
    }

    /**
     * @Route("/item/{id}", methods={"GET"}, name="get")
     */
    public function  getItem(string $id, PunkService $punkService): JsonResponse 
    {        
        $item = $punkService->getItem($id);
        
        $response = new JsonResponse();
        $response->setStatusCode($punkService->getStatusCode());
        $response->setData($item);

        return $response;
    }
}
