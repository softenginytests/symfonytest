# Symfony 4.4 punkapi

### Context

Crear 2 endpoints para testear synfony y punkapi

### Pasos instalación

* Clonar repositorio
* Ejecutar composer instar ``Composer install``
* Inicializar servidor (solo en local) ``symfony server:start``
* Abrir url (por defecto http://127.0.0.1:8000)

### Endpoints

* http://127.0.0.1:8000/items?food=milk -> Listar los datos segun el parámetro
* http://127.0.0.1:8000/item/2 -> Muestro un elemento con id 2


